from dataclasses import asdict
import os
import sys

sys.path.append(".")

import argparse

import cv2
import numpy as np
import torch

from sensetest.sensetest import ImgMetrics


def run_single(img_path: str):
    """Test wrapper for img metrics"""
    rgb_image = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)
    rgb_image = rgb_image.astype(np.float32) / 255

    # Add Gaussian Noise to the image
    mean = 0
    var = 10
    sigma = var ** 0.5
    gaussian = np.random.normal(mean, sigma, rgb_image.shape).astype(np.float32)

    noisy_image = rgb_image + gaussian

    metrics = ImgMetrics.collect(rgb_image, np.empty(0), noisy_image, torch.device("cpu"))

    print(asdict(metrics))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="ImgMetrics testing & Reference Metrics testing - Single image only"
    )

    parser.add_argument("--test_img", type=str, help="Path to image used for testing")

    args = parser.parse_args()

    run_single(args.test_img)
