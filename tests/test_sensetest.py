import os
import sys

sys.path.append(".")

import argparse

import cv2
import numpy as np

from sensetest.sensetest import ImTest

"""
Example Script to run sensetest on a single directory or a set of dirs

All reports will be stored under _reports in your local directory
"""


def run_single(test_name: str, test_dir: str, chart: str, metrics: str, imatest_key_file: str = ""):
    """Test run for denoising benchmark"""

    imtest = ImTest(name=test_name)

    for f in os.listdir(test_dir):
        path = f"{test_dir}/{f}"
        rgb_image = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)
        rgb_image = rgb_image.astype(np.float32) / 255
        imtest.collect(
            name=f.split(".")[0],
            img_out=rgb_image,
        )

    imtest.run_imatest(chart=chart, metrics=metrics, key_file=imatest_key_file)
    metrics = imtest.close(save=True)


def run_batch(args):

    for f_dir in os.listdir(args.test_dir):
        path_to_dir = f"{args.test_dir}/{f_dir}"
        run_single(
            test_name=f_dir,
            test_dir=path_to_dir,
            chart=args.chart,
            metrics=args.metrics,
            imatest_key_file=args.imatest_key_file,
        )


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Main Testing - ImTest & SenseTest")

    parser.add_argument("--test_dir", type=str)
    parser.add_argument(
        "--name",
        type=str,
        help="Name of the test, \
                if batch_processing will use name of folder by default",
    )
    parser.add_argument("--do_batch_processing", action="store_true")

    parser.add_argument("--chart", type=str, default="color-checker")
    parser.add_argument("--metrics", type=str, default="YSNR")
    parser.add_argument("--imatest_key_file", type=str, default="")

    args = parser.parse_args()

    if args.do_batch_processing:
        run_batch(args)
    else:
        run_single(args.name, args.test_dir, args.chart, args.metrics, args.imatest_key_file)
