from collections import defaultdict
from dataclasses import asdict, dataclass, field
import json
import os
from os.path import abspath, dirname
import shutil
import sys
from typing import Union

import cv2
import numpy as np
import torch

from sensetest.imatest_client import ImatestProxy
from sensetest.metrics import LPIPS, MSSSIM, PSNR, SSIM, Accutance, Contrast
from sensetest.params import Params
from sensetest.utils import validate_np, validate_torch


@dataclass
class ImgMetrics:

    accutance_out: float = field(init=False, default=0.0)
    contrast_out: float = field(init=False, default=0.0)
    accutance_ref: float = field(init=False, default=0.0)
    contrast_ref: float = field(init=False, default=0.0)
    accutance_in: float = field(init=False, default=0.0)
    contrast_in: float = field(init=False, default=0.0)

    ssim: float = field(init=False, default=0.0)
    msssim: float = field(init=False, default=0.0)
    psnr: float = field(init=False, default=0.0)
    lpips: float = field(init=False, default=0.0)

    @classmethod
    def test(
        cls, img_out: torch.Tensor, img_in: torch.Tensor, img_ref: torch.Tensor, device: Params
    ):
        """Collects test metrics and returns itself"""
        raise NotImplementedError

    @classmethod
    def collect(
        cls, img_out: torch.Tensor, img_in: torch.Tensor, img_ref: torch.Tensor, device: str
    ):
        self = cls()

        # Measure Non Reference Metrics
        if img_out.size:
            img_out_torch = validate_torch(img_out)
            img_out_np = validate_np(img_out)
            self.accutance_out = Accutance(device).forward(img_out_torch).item()
            self.contrast_out = Contrast(mode="RMS").forward(img_out_np)
        if img_ref.size:
            img_ref_torch = validate_torch(img_ref)
            img_ref_np = validate_np(img_ref)
            self.accutance_ref = Accutance(device).forward(img_ref_torch).item()
            self.contrast_ref = Contrast(mode="RMS").forward(img_ref_np)
        if img_in.size:
            img_in_torch = validate_torch(img_in)
            img_in_np = validate_np(img_in)
            self.accutance_in = Accutance(device).forward(img_in_torch).item()
            self.contrast_in = Contrast(mode="RMS").forward(img_in_np)

        # Measure reference metrics
        if img_ref.size:
            self.ssim = SSIM().forward(img_out_torch, img_ref_torch).item()
            self.msssim = MSSSIM().forward(img_out_torch, img_ref_torch).item()
            self.psnr = PSNR().forward(img_out_torch, img_ref_torch).item()
            self.lpips = LPIPS(img_out_torch, img_ref_torch).item()

        print(self.lpips)
        return self


@dataclass
class ImTest:

    name: str = ""  # Name of the test
    mode: str = "benchmark"

    metrics: dict = field(
        default_factory=lambda: {"imgs": defaultdict(dict), "mean": defaultdict(float)}
    )

    index: int = 0

    IQ_DATASETS: list = field(default_factory=list)

    params: Params = field(default_factory=lambda: Params())

    imatest = None

    def __post_init__(self):

        if not self.mode in ["benchmark", "test"]:
            raise ValueError("Incorrect mode, either benchmark or test")

        # self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.device = torch.device("cpu")

        if self.params.imatest:
            self.imatest = ImatestProxy()
            os.makedirs("tmp", exist_ok=True)
        # Create report path
        self.report_path = "_reports/metrics" if not self.name else f"_reports/{self.name}"
        os.makedirs(self.report_path, exist_ok=True)

        print("Sense Test testing suite started")

    def validate_input(self, img: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
        """Checks input images"""

        if not img.size:
            return

        if img.min() < 0 and img.max() > 1:
            raise Exception("Image should in range [0, 1]")
        # if img.dtype != np.float32:
        #    raise Exception("Image should be 32-bit floating point")
        if not isinstance(img, torch.Tensor) and not isinstance(img, np.ndarray):
            raise Exception(
                f"Image should be either numpy array or torch tensor, given {type(img)}"
            )

    def get_IQ_images(self, dataset: str) -> list:
        """Returns the IQ images for the given dataset option
        dataset: XM or GOOGLE
        returns RGB numpy arrays [0, 255]
        """
        if dataset not in self.IQ_DATASETS:
            raise Exception(f"Supported IQ dataset {self.IQ_DATASETS}")

        path_to_images = f"data/IQ/{dataset}"
        images: list = []
        for img_name in os.listdir(path_to_images):
            img_path = f"{path_to_images}/{img_name}"
            np_img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)
            images.append(np_img)
        return images

    def collect(
        self,
        name: str = "",
        img_out: Union[torch.Tensor, np.ndarray] = np.empty(0),
        img_in: Union[torch.Tensor, np.ndarray] = np.empty(0),
        img_ref: Union[torch.Tensor, np.ndarray] = np.empty(0),
    ):
        """Collect & Collate Image Metrics
        Args types:
            img: Torch Tensor or numpy array, range [0, 1], np.float32, [C, H, W] if torch and [H, W, C]
            name: name to give the image at collection time
        img_out: the returned image from the test run
        img_in: the input image for the test run
        igm_ref: the reference/groundtruth image for the test run, if a reference
                image is provided we can run reference metrics: PSNR / MSSSIM
        Acts as a persistent class that supports looping. We will accept batches later on
        """

        self.validate_input(img_out)
        self.validate_input(img_in)
        self.validate_input(img_ref)

        if not img_ref.size:
            print("Reference image provided, running reference quality metrics")

        # Collect Image metrics
        key = self.index if not name else name
        print(f"Collecting Image metrics for {key}")
        if self.mode == "benchmark":
            self.metrics["imgs"][key] = asdict(
                ImgMetrics.collect(img_out, img_in, img_ref, self.device)
            )
        elif self.mode == "test":
            self.metrics["imgs"][key] = asdict(
                ImgMetrics.collect(img_out, img_in, img_ref, self.device)
            )

        # Collect Mean Metrics
        for k in self.metrics["imgs"][key].keys():
            self.metrics["mean"][k] += self.metrics["imgs"][key][k]

        # Store return image for imatest
        if self.imatest:
            img_save = validate_np(img_out)
            cv2.imwrite(f"tmp/{key}.png", cv2.cvtColor(np.uint8(img_save * 255), cv2.COLOR_BGR2RGB))

        self.index += 1

    def run_imatest(self, chart: str = "color-checker", metrics: str = "YSNR", key_file: str = ""):
        """Wrapper around calc_metrics in imatest proxy"""

        self.imatest.validate_chart_and_metric(chart, metrics)

        self.imatest.calc_metrics(chart, metrics, "tmp", output_path=self.report_path)

        if not (key_file and key_file.endswith(".json")):
            folder_path = dirname(dirname(abspath(__file__)))
            key_file = f"{folder_path}/data/imatest_keys.json"

        with open(key_file) as f:
            chart_keys = list(json.load(f)[chart].values())[0]

        for img in self.metrics["imgs"].keys():
            ext = self.imatest.report_ext(chart, metrics)
            with open(f"{self.report_path}/{img}_{ext}") as f:
                results_key = "jsonResults" if metrics != "edge_MTF" else "sfrResults"
                imatest_report = json.load(f)[results_key]

            for k in chart_keys:
                self.metrics["imgs"][img][k] = imatest_report[k]

    def close(self, save: bool = False, meta: dict = {}) -> dict:
        """Finalizes collected metrics"""
        for k in self.metrics["mean"].keys():
            self.metrics["mean"][k] = self.metrics["mean"][k] / (self.index + 1)

        self.metrics["imgs"] = dict(self.metrics["imgs"])
        self.metrics["mean"] = dict(self.metrics["mean"])
        self.metrics["count"] = self.index
        self.metrics["name"] = self.name
        self.metrics["meta"] = meta

        if save:
            with open(f"{self.report_path}/metrics.json", "w") as out:
                json.dump(self.metrics, out, indent=4, separators=(",", ": "))

        shutil.rmtree("tmp")

        return self.metrics


if __name__ == "__main__":

    # Example for running SenseTesting suite
    # First principle - ImTest is a 'suite'

    metrics = ImTest(name="SenseTest")  # Init with default parameters

    metrics.collect("Image 1203", img1_out, img1_in)
    metrics.collect("Image 1204", img2_out, img2_in)
    metrics.collect("Image 1205", img3_out, img3_in)

    metrics.close(save=True)
